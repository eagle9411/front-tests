const state = {
  favs: []
}

const actions = {
  rehidrateFavCounter({ commit }) {
    const favItems = localStorage.getItem('favCounter');

    if(favItems) {
      commit('addRangeFavElements', JSON.parse(favItems))
    }
  },
  toggleFav({ commit, state }, gifID) {
    if(!state.favs.includes(gifID)) {
      commit('addFavElement', gifID)
    } else {
      commit('removeFavElement', gifID)
    }
  }
}

const mutations = {
  addRangeFavElements(state, payload) {
    state.favs.push(...payload);
  },
  addFavElement(state, payload) {
    state.favs.push(payload);

    localStorage.setItem('favCounter', JSON.stringify(state.favs))
  },
  removeFavElement(state, payload) {
    state.favs = state.favs.filter(p => p != payload)

    localStorage.setItem('favCounter', JSON.stringify(state.favs))
  }
}

const getters = {
  favCounter: (state) => state.favs.length,
}



const namespaced = true


export default {
  state,
  mutations,
  actions,
  namespaced,
  getters
}