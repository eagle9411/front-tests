import Vue from 'vue';
import Vuex from 'vuex';
import FavStore from '@/store/Fav.store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    FavStore
  }
})
