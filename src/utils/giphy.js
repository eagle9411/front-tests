import { GiphyFetch } from '@giphy/js-fetch-api';

const giphy = new GiphyFetch(process.env.VUE_APP_GIPHY_API_KEY);


export default giphy;