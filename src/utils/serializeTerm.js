const serializeTerm = (term) => {
  return term.replaceAll(' ', '-').toLowerCase();
}

export default serializeTerm