const colorRandom = () => {
  const colors = ['#00ff99','#9933ff', '#00ccff', '#fff35c', '#ff6666']

  const randomIndex = parseInt(Math.random() * ((colors.length - 1) - 1))
  return colors[randomIndex]
}


export default colorRandom