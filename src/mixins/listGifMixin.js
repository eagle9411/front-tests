import { mapActions, mapState } from 'vuex';

export default {
  methods: {
    ...mapActions('FavStore', ['toggleFav'])
  },
  computed: {
    ...mapState('FavStore', ['favs'])
  }
}