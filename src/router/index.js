import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/search/:term',
    name: 'Search',
    component: () => import('@/views/Search/Search.vue')
  },
  {
    path: '/favs',
    name: 'Favs',
    component: () => import('@/views/Favs/Favs.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
