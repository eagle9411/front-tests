# Diario de trabajo

## Miercoles 17 de Noviembre

Hoy inicié la prueba a pesar de que el correo lo recibí el Martes. Casualmente hoy es mi cumpleaños. Partí del hecho de que no tengo una UI diseñada y en la cual trabajar, teniendo en cuenta esto primero decidí investigar sobre Giphy y como obtener acceso a su API, y todo lo que con lleva.

Encontré que Giphy ya tiene elementos desarrollados para hacer la integración aun mas rápida. Así que decidí usar su modulo que desarrollaron para interactuar con su API. No se si fue la mejor decisión, pero asumiré que sí, porque podría haber simplemente hecho una petición fetch o instalar axios para hacer las peticiones.

Con lo anterior me puse a probar como funciona el API y que datos devuelve para empezar a imaginar una Interfaz. Siendo sincero no tengo mucha imaginación al nivel UI (Por algo soy programador). Así que tomare la base la interfaz actual de la plataforma. Color negro de fondo y colores en los elementos de la interfaz.
Al menos hoy me pude dar una idea de que componentes necesito y crear los primeros. Cree un Gif que será la representación del objeto que devuelve el API y un Grif que será un grid que mostrara los Gif.

Grif?. Grid + Gif = Grif. Original no?

## Jueves 18 de Noviembre

Decidí continuar con el camino que marqué ayer. He construido componentes que me servirán para complementar la UI y su estructura. Navbar, Logo, etc...

Creo que tengo una mejor idea la interfaz que quiero hacer. La verdad es que muy probablemente me ando inspirando en el sitio oficial de Giphy. (Uso educativo, no comercial. Por si acaso).

Ya generé una estructura para las búsquedas y ya almaceno las búsquedas en el localstorage. Creo que parte del reto ya lo tengo resuelto. Solo faltara el tema de los Fav. y tal vez estilizar un poco mejor la plataforma.
