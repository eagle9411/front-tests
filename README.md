# Bienvenido a la prueba de Frontend.

La prueba la realice con Vue.js. Utilizando Tailwind como Framework CSS.

## Diario de trabajo

Senti la necesidad de realizar un diario de trabajo primero porque muy seguramente esto lo hare en los momentos libres que tenga durante los 5 dias, ademas que si se toman el tiempo podrian entender lo que paso por mi mente al desarrollar.
[Diario](./README.md)

## Levantar ambiente

El proyecto solamente tiene una variable de entorno que es el API Key que se utiliza para la integración con Giphy. De igual manera subi un .env.example para que se pueda ver la estructura.

El API KEY es el siguiente: BiybPgKfro2KWodihsMsKPmzMP179G59

OJO: Lo pongo en el repositorio por el simple hecho de informar sobre el dato. Entiendo que es una mala practica exponer este tipo de datos aunque sean Public Keys.

## Problematica

Construye un Giphy client que busque perritos, de preferencia en Vue.js

* Responsivo.
* Utiliza componentes reutilizables.
* Maneja animaciones que le den ganas al usuario de regresar.
* Guardar Mis búsquedas anteriores en localstorage.
* Sube tu propuesta al repositorio como Merge Request.

Opcionalmente:

* Usa tailwind para definir los estilos de tu componentes.

## Explicación de solución

Basicamente tome la interfaz de Giphy como base, primero porque me cuesta trabajo imaginar interfaces de cero. Tal vez porque llevo un par de años trabajando junto a un UI/UX Designer y me ha evitado pensar en esto.

Cree componentes basados en la data que devuelve el API. Muy probablemente estos componentes no sean reutilizables, si lo vemos del punto de una liberia de componentes. Pero si me permitieron reutilizarlo para las pantallas que cree. 

Realice la integración del API con una libreria que giphy da para el desarrollo web. Ya que redacto esto, siento que fue algo de trampa. Tal vez con esto querian evaluar mi capacidad para utilizar alguna funcionalidad de comunicación con un Web API. Pero funcionalmente me permitio desarrollar de manera mas rapida, ya que no tuve necesidad de revisar la documentación del API. Sino que con lo que documentan en el modulo pude hacer toda la integración. El modulo se llama [@giphy/js-fetch-api
](https://github.com/Giphy/giphy-js/blob/master/packages/fetch-api/README.md).

Tal vez noten que Utilizo PascalCase en los componentes, y esto lo justifico con que al escribirlos de esa manera se destacan mejor al estar leyendo y se identifican mejor cuales son los componentes.

Tambien utilizo las abreviaciones para el bind y los eventos. Creo que si lo use de manera homogenea y esto tambien ayudan a leer mejor los componentes.

Creo que intente usar lo mayor posible todo de Vue. Incluso utilice Vuex para el tema de los favoritos. Estoy contento con el resultado mas no satisfecho, siento que lo pude mejorar aun más.

## Que pude hacer mejor

Creo que la representación del GIF en la interfaz, es algo que pude mejorar. Ya que no todos los Gifs tienen las mismas dimensiones lo que dificulta un poco "pintarlos" de manera homogenea. Por eso en el componente TopOfGif los acomode de tal manera que tengan la misma altura y el gif se vea de manera decente, pero podria haberlo hecho mejor.

## Me sorprendi

Hace mucho que no hacia un componente sin meter clases y estilos propios. Volver a usar tailwind me permitio avanzar mas rapido y sobre todo tener codigo un poco más limpio.

Es una gran libreria CSS. Aunque a veces se siente que tiene demasiadas cosas, es muy util.